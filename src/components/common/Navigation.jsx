import AnimatedLogo from "../AnimatedLogo";
export default () => {
  return (
    <nav className="w-full sticky flex px-[24px] border-bottom-[2px] border-black bg-white py-[24px]">
      <AnimatedLogo className="w-[16px] h-[16px]" />
    </nav>
  );
};
