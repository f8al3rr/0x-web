import AnimatedLogo from "../components/AnimatedLogo";
import Navigation from "../components/common/Navigation";
function Splash() {
  return (
    <div className="splash w-screen h-[200%] bg-orange-300">
      <Navigation />
      {/* <AnimatedLogo /> */}
    </div>
  );
}

export default Splash;
