import React from "react";
import ReactDOM from "react-dom/client";
import Splash from "./pages/Splash";
import "virtual:windi.css";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(<Splash />);
